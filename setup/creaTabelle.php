<?php
$servername='localhost';
$username='root';
$password='';
$dbname='platformc';//$dbname='my_olleirac';
$conn=new mysqli($servername,$username,$password)
or die ("Impossibile connettersi al server ");

$creaDB="CREATE DATABASE IF NOT EXISTS $dbname";
if ($conn->query($creaDB)) {
print ("Creazione del database effettuata!<br>");
} else {
echo "Errore durante la creazione del database: <br><br>";
}
$conn->close();


$conn=new mysqli($servername,$username,$password,$dbname);
$createGruppi = "CREATE TABLE IF NOT EXISTS gruppi (   
  idgruppo varchar(15) PRIMARY KEY NOT NULL,
  anno int(4) NOT NULL,
  groupass varchar(15) NOT NULL,
  cogn1  varchar(25) NOT NULL,
  nome1  varchar(25) NOT NULL,
  email1 varchar(45) NOT NULL, 
  matr1  varchar(25) NOT NULL,  
  cogn2  varchar(25),
  nome2  varchar(25),
  email2 varchar(45),
  matr2  varchar(25) 
)ENGINE=INNODB;";
if ($conn->query($createGruppi)) {
print ("Creazione della tabella Gruppi effettuata!<br>");
} else {
echo "Errore durante la creazione della tabella Gruppi <br><br>";		
}
$conn->close();



$createScripts = "CREATE TABLE IF NOT EXISTS elaborati (   
  idelaborato INT(6) AUTO_INCREMENT PRIMARY KEY,
  nomefile char(20),
  titolo text NOT NULL,  
  dataConsegna date,
  anno int(4),
  materiale char(50),
  note char(50),
  attivo tinyint(1),
  max_cons tinyint(1)  	
)ENGINE=INNODB;";
$conn=new mysqli($servername,$username,$password,$dbname);
if ($conn->query($createScripts)) {
print ("Creazione della tabella Elaborati effettuata!<br>");
} else {
echo "Errore durante la creazione della tabella Elaborati<br><br>";		
}
$conn->close();

$createDeliveries = "CREATE TABLE IF NOT EXISTS consegne (  
  idcons int(6) AUTO_INCREMENT PRIMARY KEY,
  cod_gruppo varchar(15),
  cod_elaborato int (6),
  dataConsegna date,
  nomeFile varchar(15),
  voto int(2),
  out_compilazione text,
  out_test text,
  similarity text,
  CONSTRAINT FK_gruppo FOREIGN KEY (cod_gruppo)
  REFERENCES gruppi(idgruppo),
  CONSTRAINT FK_elaborato FOREIGN KEY (cod_elaborato)
  REFERENCES elaborati(idelaborato) 
) ENGINE=INNODB;";
$conn=new mysqli($servername,$username,$password,$dbname);
if ($conn->query($createDeliveries)) {
print ("Creazione della tabella Consegne effettuata!<br>");
} else {
echo "Errore durante la creazione della tabella Consegne<br><br>";		
}
$conn->close();

function sheetData($sheet,$date,$year) {
	$servername='localhost';
	$username='root';
	$password='';
	$dbname='platformc';//$dbname='my_olleirac';
	$cont=0;
	$x = 2;//$x = 1; per saltare la riga di intestazione 
	while($x <= $sheet['numRows']) {
		/* per settare correttamente la data iniziale la chiamata dal main considera la giornata di ieri,
		di conseguenza e' necessario incrementare la data per ogni riga letta dal file elaborati*/
		$date = date("Y-m-d", strtotime("$date +1 day"));
		$y = 2;// per saltare la colonna idelaborato
		$instance=array();
		while($y <=$sheet['numCols']) {			
			$cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
			array_push($instance,$cell);				
			$y++;
		}  
		//echo "<pre>";print_r($instance);echo"</pre>";
		$mysqli=new mysqli($servername,$username,$password,$dbname);
		if ($mysqli->connect_error) {
						die("Impossibile connettersi al database: " . $mysqli->connect_error);
		}
		// nomefile,titolo,dataConsegna,anno,materiale,note,attivo,max_cons 
		//	   1	   2	  3		     4		5	    6	  7		 8		  		
		$sql="INSERT INTO elaborati (nomefile,titolo,dataConsegna,anno,materiale,note,attivo,max_cons)
		VALUES ('$instance[0]','$instance[1]','$date',
					  $year,'$instance[4]','$instance[5]',$instance[6],$instance[7])";		  
		if($result = mysqli_query($mysqli, $sql))
		{
			$cont++;//echo"<br>Inserimento effettuato!<br>";
		}
					else {echo "Errore: " . $sql . "<br>" . $mysqli->error;}
			
		mysqli_close($mysqli);	
    $x++;
  }	
  if ($cont>0){
	  echo"<br>Inserimento effettuato di n. $cont elaborati!<br>";
  }
}



/*
in relazione alla tabella elaborati
il campo titolo contiene il titolo dell'elaborato
quello testo contiene invece il file in formato pdf da visualizzare/scaricare
materiale contiene eventuali file .h che il docente mette a disposizione 
*/



include 'excel_reader.php';     // include the class
// from: https://coursesweb.net/php-mysql/read-excel-file-data-php_pc
// creates an object instance of the class, and read the excel file data
$excel = new PhpExcelReader;
$excel->read('elaborati.xls');
$nr_sheets = count($excel->sheets);       // gets the number of sheets

$oggi = date("Y-m-d");
$date = date("Y-m-d", strtotime("$oggi -1 day"));
$year=date("Y");

// Excel file data is stored in $sheets property, an Array of worksheets
/*
The data is stored in 'cells' and the meta-data is stored in an array called 'cellsInfo'

Example (firt_sheet - index 0, second_sheet - index 1, ...):

$sheets[0]  -->  'cells'  -->  row --> column --> Interpreted value
         -->  'cellsInfo' --> row --> column --> 'type' (Can be 'date', 'number', or 'unknown')
                                            --> 'raw' (The raw data that Excel stores for that data cell)
*/

// this function creates and returns a HTML table with excel rows and columns data
// Parameter - array with excel worksheet data


// traverses the number of sheets and sets html table with each sheet data in $excel_data
for($i=0; $i<$nr_sheets; $i++) {
  //$excel_data .= '<h4>Sheet '. ($i + 1) .' (<em>'. $excel->boundsheets[$i]['name'] .'</em>)</h4>'. sheetData($excel->sheets[$i]) .'<br/>';  
  sheetData($excel->sheets[$i],$date,$year);	
}



/*
Text = 65.535 caratteri
Longtext = 4.294.967.925 caratteri

Da una ricerca in rete sembra non sia possibile memorizzare
un testo formattato RTF in un campo della tabella


per impostare una chiave primaria composta da due campi:

CREATE TABLE person (
   P_Id int ,
   ............,
   ............,
   CONSTRAINT pk_PersonID PRIMARY KEY (P_Id,LastName)
);
*/


?>