 <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Portale Programmazione CL.B</title>
      <!-- Fogli di stile -->
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
      <link rel="stylesheet" href="../plugins/fancybox/jquery.fancybox.css">
      <link rel="stylesheet" href="../plugins/flexslider/flexslider.css">
      <link rel="stylesheet" href="../css/stili-custom.css">
      <!-- Modernizr -->
      <script src="assets/js/modernizr.custom.js"></script>
      <!-- respond.js per IE8 -->
      <!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- Header e barra di navigazione -->
      <header>
  <nav class="navbar navbar-default">
  <div class="container">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="index.html">Carlo</a> -->
   </div>
   <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav">
	 <li><a href="../../index.html">Home</a></li>
      <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
		<!--  glyphicon glyphicon-file -->
		  <li><a href="process_login.php"><span class="glyphicon glyphicon-file"></span> Consegna</a></li>
		  <!--<li><a href="process_login.php"><span class="glyphicon glyphicon-upload"></span> Consegna</a></li> -->
          <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
   </div><!-- /.nav-collapse -->
  </div>

		 <!-- Barra testata di pagina -->
	<div class="header-portfolio clearfix">
	<h2 class="pull-left">Portale del corso di Programmazione Classe-B: Area Riservata &#8594; Situazione elaborati</h2>
	<ul class="breadcrumb pull-right">

	</ul>
	</div><!-- /header-portfolio -->
 </nav><!-- /.navbar -->
  </header><!-- /header -->

<?php
//session_start();
include 'db_connect.php';
include 'functions.php';
sec_session_start();

$mysqli;
  // Check connection
    if (mysqli_connect_errno())
    {
        echo "Errore durante la connessione al database.";
		exit();
    }



$query ="SELECT
		elaborati.idelaborato,elaborati.titolo,elaborati.anno,
		consegne.cod_gruppo,consegne.cod_elaborato,consegne.dataConsegna,consegne.nomeFile,
		consegne.voto,consegne.out_compilazione,consegne.out_test,consegne.similarity
		FROM elaborati,consegne
		WHERE consegne.cod_gruppo='".$_SESSION['groupid']."'
			and consegne.cod_elaborato=elaborati.idelaborato ";


	/*
	$query="SELECT * FROM elaborati,consegne
			WHERE consegne.cod_gruppo='".$_SESSION['idgruppo']."'
			and consegne.cod_elaborato=elaborati.idelaborato
	";*/

if ($result = mysqli_query($mysqli,$query)) {

    /* determine number of rows result set */
    $row_cnt = mysqli_num_rows($result);
	if($row_cnt==0)
	{
		echo "<script>alert('Il gruppo non ha ancora effettuato consegne di elaborati')</script>";
	}
	else
	{
    echo "<style>
		table {

				margin: 2.5%;
				width: 95%;
				box-shadow: 0 0 20px rgba(0,0,0,.4);
				border: 1px solid #116b6a;
        }

    th {
        text-align: center;
        background-color: #b32929;
        color: white;
        font-weight: bold;
        }

		th, td {
				padding: 15px;
				border: 1px solid;
				text-align: center;
				/*border-left: 0;
				border-right: 0;
			  }

			  td {
				background-color: white;
			  }

		</style>";
    echo "<center><font size='6'><strong><i>Riepilogo degli elaborati consegnati</i></strong></font></center>";
    echo "<br><table border='1' align='center' valign=''middle'>\n";

  	echo "<tr>
    <th>Id</th>
    <th>Titolo</th>
    <th>File consegnato</th>
  	<th>Data consegna</th>
    <th>Anno</th>
  	<th>Voto</th>
    <th>Compilazione</th>
  	<th>Test</th>
    <th>Similarity</th>
  	 </tr>";
	while($row = $result->fetch_assoc()) {
		$request=array();


		echo "<tr>";
		$formatDate=date("d-m-Y", strtotime($row['dataConsegna']));
		$year=date('Y');

		array_push($request,$row['cod_elaborato'],$formatDate);
		//echo"<pre>";print_r($istanza);echo"</pre>";
		array_push($_SESSION['scripts'],$request);
		echo "<td align='middle'>".$row['cod_elaborato']."</td>
			  <td align='middle'>".$row['titolo']."</td>
			  <td align='middle'>".$row['nomeFile']."</td>
			  <td align='middle'>".$formatDate."</td>
			  <td align='middle'>".$row['anno']."</td>
			  <td align='middle'>".$row['voto']."</td>";
			 // <td>".$row['out_compilazione']."</td>
		//$path='../../consegne/'.$year.'/';$file=$row['nomeFile'];$Link=$path.$file;
		$scriptDir="esercizio".$row['cod_elaborato'].'/';
		$logpath = '../../consegne/'.$year.'/'.$_SESSION['groupid'].'/';
		$logfile=$logpath.$scriptDir.$row['nomeFile'].'.txt';
		if ($row['out_compilazione']==0){
			echo"<td align='middle'><img alt='compilazione effettuata con successo' src='../img/green.jpg' height='22' width='22'></td>";
		}
		else{
			echo"<td align='middle'><a href='".$logfile."' target='_blank'>
						<img alt='Errore durante la compilazione' src='../img/red.jpg' height='22' width='22'></a></td>";
		}
		echo" <td align='middle'>".$row['out_test']."</td>
			  <td align='middle'>".$row['similarity']."</td>
			  ";

		//$v=	$row['idelaborato'];
		echo "</tr>";
	}
	echo "</table>\n";

  echo"

     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <strong><font size=4>Legenda</font size></strong><br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <img src='../img/green.jpg' height='14' width='14'>&nbsp;&nbsp;&nbsp;<font size=3>Compilazione eseguita correttamente</font><br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <img src='../img/red.jpg' height='14' width='14'>&nbsp;&nbsp;&nbsp;<font size=3>Errore durante la compilazione</font><br><br>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <font size=3><i>Fare click sul semaforo rosso per visualizzare gli errori rilevati dal compilatore C</i></font><br><br>";


	/*
	echo "<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<sup>*</sup>
	Esito: 0 = Compilazione effettuata con successo, 1= Errori durante la compilazione<br><br></div>";*/
	}

    /* close result set */
    mysqli_free_result($result);
}

/* close connection */
mysqli_close($mysqli);


?>

<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>

<!-- jQuery e plugin JavaScript  -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>-->
<script src="assets/js/scripts.js"></script>
</body>
</html>
