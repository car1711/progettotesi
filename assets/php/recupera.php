<html>
    <head>
	   <title>Platform C</title>
	   <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- <title>Consegna esercizi di programmazione</title> -->
<!-- Fogli di stile -->
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
<!--<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">-->
<link rel="stylesheet" href="../css/stili-custom.css">
<!-- Modernizr -->
<script src="../js/modernizr.custom.js"></script>
<!-- respond.js per IE8 -->
<!--[if lt IE 9]>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
	</head>
    <body>
	<!-- Header e barra di navigazione -->
<header>
<nav class="navbar navbar-default">
<div class="container">
 <div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
  </button>
  <!--<a class="navbar-brand" href="index.html">Carlo</a>-->
 </div>
 <div class="collapse navbar-collapse navbar-responsive-collapse">
	 <ul class="nav navbar-nav">
	 <li><a href="../../index.html">Home</a></li>
      <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
     </ul>
   </ul>
  <ul class="nav navbar-nav navbar-right">
 <li><a href="login.php?registrato=1" style="text-decoration:none"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
 <li><a href="login.php?registrato=0" style="text-decoration:none">Crea un gruppo</a></li>
  </ul>
 </div><!-- /.nav-collapse -->
</div>

<!-- Barra testata di pagina -->
<div class="header-portfolio clearfix">
 <h2 class="pull-left">Unibo on Platform C - Portale del corso di Programmazione Classe-B</h2>
 <ul class="breadcrumb pull-right">
  <!--  <li><a href="../../index.html">Home</a></li>
  <li class="active">Login</li>  -->
</ul>
</div><!-- /header-portfolio -->
</nav><!-- /.navbar -->
</header><!-- /header -->
<!-- Contenuti (griglia) -->
<div class="container">
<!-- Lead presentazione -->
<section id="presentazione">
<div class="row">
<div class="col-sm-12">
 <h1 class="text-center"><small><strong></strong></small></h1>

</div><!-- /.col-sm-12 -->
</div><!-- /.row -->
</section><!-- /section presentazione -->


</div><!--  /.container -->
<!-- jQuery e plugin JavaScript  -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>-->
<script src="../js/scripts.js"></script>

<?php
//session_start();
include 'db_connect.php';
include 'functions.php';
sec_session_start();
$email=$_POST['rec_email'];
$mysqli;

$sql = "SELECT * FROM gruppi WHERE email1='$email' or email2='$email'";
if($result = mysqli_query($mysqli, $sql)){
	if(mysqli_num_rows($result) > 0){
		$group = mysqli_fetch_array($result);
		$to=$email;
		$subject='Unibo On Platform C - reinvio password';
		$message="Gentile studente " ;
		$message.="Ti inviamo l'id del tuo gruppo: ".$group['idgruppo']." e la relativa password: "
					.$group['groupass'];
		$headers='From: Unibo On Platform C' . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($to, $subject, $message, $headers);
		//echo "<br><h2><center>Inoltro effettuato, come richiesto</center></h2>";
		echo "<script>alert('Inoltro effettuato, come richiesto. Vai ora alla pagina di Login')</script>";
    }
	else {
			echo "<h2><center>
					L'email fornita non risulta registrata oppure non &egrave; corretta,<br>tra qualche istante sarete reindirizzati alla Home page
				  </center></h2>";
			echo"<noscript>
					<meta http-equiv='refresh' content='5;url=../../choose_login.php' />
				</noscript>";
			echo"<script type='text/javascript'>
					window.setTimeout(function() {
					window.location.href='../../choose_login.php';
					}, 5000);
				</script>";
	}
}
session_unset();
session_destroy();
/*
echo"<center><button onclick='apriIndex()'>Torna alla home page</button></center>
							  <script>
							  function apriIndex() {
    						  window.open('/./platformc/index.html');
							  }
							  </script>";*/

?>

<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>
  <!-- jQuery e plugin JavaScript  -->
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
  <script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="assets/plugins/validation/jquery.validate.js"></script>
  <script src="assets/js/scripts.js"></script>
  <script type="text/javascript" src="assets/js/sha512.js"></script>
  <script type="text/javascript" src="assets/js/forms.js"></script>

</body>
</html>
