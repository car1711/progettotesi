 <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Portale Programmazione CL.B</title>
      <!-- Fogli di stile -->
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
      <link rel="stylesheet" href="../plugins/fancybox/jquery.fancybox.css">
      <link rel="stylesheet" href="../plugins/flexslider/flexslider.css">
      <link rel="stylesheet" href="../css/stili-custom.css">
      <!-- Modernizr -->
      <script src="assets/js/modernizr.custom.js"></script>
      <!-- respond.js per IE8 -->
      <!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- Header e barra di navigazione -->
      <header>
  <nav class="navbar navbar-default">
  <div class="container">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="index.html">Carlo</a> -->
   </div>
   <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav">
	 <li><a href="../../index.html">Home</a></li>
      <li><a href="https://www.unibo.it/sitoweb/alessandra.lumini">Contatti</a></li>  
     </ul>
     <ul class="nav navbar-nav navbar-right">
          <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
   </div><!-- /.nav-collapse -->
  </div>
 
		 <!-- Barra testata di pagina -->
	<div class="header-portfolio clearfix">
	<h2 class="pull-left">Pagina ...</h2>
	<ul class="breadcrumb pull-right">
		
	</ul>
	</div><!-- /header-portfolio -->
 </nav><!-- /.navbar -->
  </header><!-- /header -->

<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>

<!-- jQuery e plugin JavaScript  -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>-->
<script src="assets/js/scripts.js"></script>
</body>
</html>
