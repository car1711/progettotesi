 <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Portale Programmazione CL.B</title>
      <!-- Fogli di stile -->
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
      <link rel="stylesheet" href="../plugins/fancybox/jquery.fancybox.css">
      <link rel="stylesheet" href="../plugins/flexslider/flexslider.css">
      <link rel="stylesheet" href="../css/stili-custom.css">
      <!-- Modernizr -->
      <script src="assets/js/modernizr.custom.js"></script>
      <!-- respond.js per IE8 -->
      <!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
      <!-- Header e barra di navigazione -->
      <header>
  <nav class="navbar navbar-default">
  <div class="container">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="index.html">Carlo</a> -->
   </div>
   <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav">
	 <li><a href="../../index.html">Home</a></li>
      <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
          <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
   </div><!-- /.nav-collapse -->
  </div>
<?php

function adjust_log($filename, $string_to_replace, $replace_with){
    $content=file_get_contents($filename);
    $content_chunks=explode($string_to_replace, $content);
    $content=implode($replace_with, $content_chunks);
    file_put_contents($filename, $content);
}

// Compile students' script
function compileScript($scriptPath,$scriptName,$scriptExt){
	echo "<font color='white'>";
	putenv("PATH=C:/MinGW/bin");// per windows

	$sourceBlock=$scriptPath.$scriptName.".".$scriptExt;
	$objBlock= $scriptPath.$scriptName.".o";
	$exeBlock= $scriptPath.$scriptName.".exe";
	$logfile= $scriptPath.$scriptName.".txt";

	// azioni di GCC
	exec("gcc ".$sourceBlock." 2> ".$logfile);// file di log
	exec("gcc ".$sourceBlock." -o ".$objBlock);// file .obj
	exec("gcc ".$sourceBlock." -o ".$exeBlock);// file. exe

	// rimuovere dal file di log il path del file compilato
	$string_to_replace=$sourceBlock.':';
	$replace_with="";
	adjust_log($logfile, $string_to_replace, $replace_with);

	// gestione esito della compilazione
	system($exeBlock,$output);
	if ($output==1){
	  $_SESSION['compResult']=1;//echo "OKKAZZ";
	}
	else
	{
	  $_SESSION['compResult']=0;//echo "OK";
	}
	echo "</font>";
}

// Upload student script
function uploadEx($text,$year,$maxc,$tot){
/*
https://stackoverflow.com/questions/49583291/rename-file-upload-php
*/
if(isset($_POST['load'])){
  // Recupera il file col metodo Post  - Retrieve file from post method
  $file = $_FILES['file'];

  // Prende le proprieta' del file - Get file properties
  $fileName = $file['name'];//echo $fileName."<br>";
  $fileTmpName = $file['tmp_name'];//echo $fileTmpName."<br>";
  $fileSize = $file['size'];//echo $fileSize."<br>";
  $fileError = $file['error'];//echo $fileError."<br>";
  $fileType = $file['type'];//echo $fileType."<br>";

  //Separa nome ed estensione del file -  Separate name and file extension
  $fileExt = explode('.', $fileName);
  //Imposta il minuscolo - Set to always lowercase
  $fileActualExt = strtolower(end($fileExt));

  //Verifica se l'estensione del file ? accettata - Check whether file extension is allowed
  if(strcmp($fileActualExt, "c")==0){
      if($fileError === 0){
          //Verifica la dimensione del file - Check file size criteria
          if($fileSize <= 150000){
			$date = date("Y-m-d");

			//determina il numero progressivo che identifica la nuova consegna
			$del=$tot+1;//


			//$path='../../consegne/'.$year.'/'.$_SESSION['groupid'].'/'; // altra impostazione corretta del path
			$path=$_SERVER["DOCUMENT_ROOT"].'/ProgettoTesi/consegne/'.$year.'/'.$_SESSION['groupid'].'/';
			$scriptDir="esercizio".$_SESSION['scriptid'];

			if (!file_exists($path.$scriptDir))
			{
				mkdir($path.$scriptDir, 0777);// create group folder into year folder
			}
			$path=$path.$scriptDir.'/';

			  //Definisce il nomefile desiderato numero elaborato+numero consegna+idgruppo, imortante per la similarity
			  //Define your custom file name script number+delivery number+groupid, it's important during check similarity
			  $NewName = "sc".$_SESSION['scriptid']."del".$del."_".$_SESSION['groupid'];

              $fileNameNew = $NewName.".".$fileActualExt;
              //Definisce la destinazione - Define file destination
			  // $_SERVER["DOCUMENT_ROOT"] in windows punta  a-> C:/xampp/htdocs
              //$fileDestination = $_SERVER["DOCUMENT_ROOT"].$path.$fileNameNew;
			  $fileDestination = $path.$fileNameNew;
              //php uploading files
              move_uploaded_file($fileTmpName, $fileDestination);
			  $_SESSION['compResult']=0;
			  compileScript($path,$NewName,$fileActualExt);
			  //echo "Esito compilazione: ".$_SESSION['compResult'];

			  include 'db_connect.php';
			  $mysqli;
			  //if ($mysqli->connect_error) {die("Errore durante la connessione: " . $mysqli->connect_error);}
			  $sql = "INSERT INTO consegne (cod_gruppo, cod_elaborato,dataConsegna,nomeFile,out_compilazione)
					values ('".$_SESSION['groupid']."','".$_SESSION['scriptid']."','$date','$fileNameNew','".$_SESSION['compResult']."')";
			  if ($mysqli->query($sql) === TRUE) {
				echo "<script>alert('Esercizio correttamente inserito! Accedi alla Dashboard per verificare il riepilogo degli elaborati consegnati ed il relativo esito della compilazione')</script>";
				echo "<script>location.href='situazione.php';</script>";
			  } else {
					echo "<script>alert('Errore durante l'inserimento')</script>";
			  }
			  $mysqli->close();

          } else{
			  echo "<script>alert('File troppo grande!')</script>";
          }
      } else{
		  echo "<script>alert('Errore durante il caricamento')</script>";
      }
  } else{
    $msg="Errata consegna, estensione del file non ammessa!";
    echo "<font face='verdana'><center><h1>".$msg."</h1></center></font>";
    echo"<noscript>
        <meta http-equiv='refresh' content='5;url=process_login.php' />
      </noscript>";
    echo"<script type='text/javascript'>
        window.setTimeout(function() {
        window.location.href='process_login.php';
      }, 7000);
      </script>";
  }
}
else {
	echo "
			<!DOCTYPE html>
			<html>
			<head>
			<title>Carica il tuo file</title>
      <style>
  		table {

  				margin: 2.5%;
  				width: 95%;
  				box-shadow: 0 0 20px rgba(0,0,0,.4);
  				border: 1px solid #116b6a;
          }

      th {
          text-align: center;
          background-color: #b32929;
          color: white;
          font-weight: bold;
          }

  		th, td {
  				padding: 15px;
  				border: 1px solid;
  				text-align: center;
  				/*border-left: 0;
  				border-right: 0;
  			  }

  			  td {
  				background-color: white;
  			  }

  		</style>
			</head>
			<body>
<strong><center><font size='6'><i>Consegna dell'Elaborato ".$_SESSION['scriptid']."</i></font></center></strong>
			<form enctype='multipart/form-data' action='#' method='POST'>
					<table border='1' align='center'>

          <tr>
          <th>Id</th>
          <th>Titolo</th>
          <th>Anno</th>
          <th>Inserisci il file</th>
          <th></th>
           </tr>
          <tr>
          <td>".$_SESSION['scriptid']."</td>
          <td>$text</td>
          <td>$year</td>
          <td><input class='btn' type='file' name='file' required></td>
          <td><input class='btn' type='submit' name='load' value='Carica'></td>
          </tr>

					</table>
			</form>
			<div>
			<br><br><br>
			</div>
			</body>
			</html>";
}

}

/*

<tr><td>Id dell'esercizio selezionato: $_SESSION['scriptid']</td></tr>
  <tr><td>Titolo: $text</td></tr>
  <tr><td>Relativo all'anno: $year</td></tr>
  <tr><td>Carica ora il file da inviare</td></tr>
  <tr><td></td></tr>

  <tr><td><input type='file' name='file' required></td></tr>
  <tr><td><input type='submit' name='load' value='Carica'></input></td></tr> */

//session_start();
include 'db_connect.php';
include 'functions.php';
sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
	//echo "<pre>";print_r($_SESSION['scripts']);echo "</pre>";
$_SESSION['scriptid']=( isset($_REQUEST['id'] ) ) ? $_REQUEST['id'] : '';//echo "<br>".$_SESSION['scriptid']."<br>";
$i=$_SESSION['scriptid']-1;// id parte da 1 e per usarlo come valore indice del vettore si allinea decrementandolo di uno
//echo "<pre>";print_r($_SESSION['scripts']);echo "</pre>";
$maxc=$_SESSION['scripts'][$i][3];
$year=$_SESSION['scripts'][$i][2];
$text=$_SESSION['scripts'][$i][1];

echo "
	<!-- Barra testata di pagina -->
	<div class='header-portfolio clearfix'>
	<h2 class='pull-left'>&nbsp;&nbsp;&nbsp; Portale Programmazione CL.B - Pagina di consegna del ".$_SESSION['groupid']."</h2>
	<ul class='breadcrumb pull-right'>

	</ul>
	</div><!-- /header-portfolio -->
 </nav><!-- /.navbar -->
  </header><!-- /header -->";
 /*
echo"Il gruppo ".$_SESSION['idgruppo']." ha scelto di effettuare la consegna dell'esercizio:<br> ".$text.
	"<br> avente codice ".$_SESSION['scriptid']."<br>relativo all'anno ".$year."<br> in data odierna e con numero massimo di consegne pari a ".$maxc;
*/
	//echo "<script>alert('consegna dell'esercizio')</script>";
$mysqli;
if (mysqli_connect_errno())
	{
		echo "Connessione al database fallita: " . mysqli_connect_error();
	}
else
{

		$mysqli;

		/*$sql = "SELECT COUNT(*) as tot FROM consegne where
			cod_gruppo='".$_SESSION['idgruppo']."' and
			cod_elaborato='$_SESSION['scriptid']'";*/
		$sql = "SELECT cod_gruppo,cod_elaborato FROM consegne where
			cod_gruppo='".$_SESSION['groupid']."' and
			cod_elaborato='".$_SESSION['scriptid']."'";
		$queryresults = mysqli_query($mysqli,$sql);
		if($queryresults) {
			$tot=$queryresults->num_rows;
			//$rowresults = mysqli_fetch_assoc($queryresults);
			//echo "<pre>";print_r($rowresults);echo "</pre>";echo "<br>".$rowresults['tot']."<br>";
			//if ($rowresults['tot']<$maxc){
				//$tot=sizeof($rowresults);
			if ($tot<$maxc){
				uploadEx($text,$year,$maxc,$tot);
			}
			else {
				echo "<script>alert('Attenzione, elaborato non consegnabile per raggiungimento del limite, numero consegne effettuate= '+$tot)
				</script>";
				echo "<script>location.href='process_login.php';</script>";
			}

		}/* else {

			echo "<script>alert('La tabella Consegne risulta vuota')
				</script>";
			uploadEx($gruppo,$_SESSION['scriptid'],$maxc,0);

		}*/
}

?>
<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>

<!-- jQuery e plugin JavaScript  -->
<script src="http://code.jquery.com/jquery.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<!--<script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>-->
<script src="assets/js/scripts.js"></script>
</body>
</html>
