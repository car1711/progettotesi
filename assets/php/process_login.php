 <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Portale Programmazione CL.B</title>
      <!-- Fogli di stile -->
      <link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
      <link rel="stylesheet" href="../css/stili-custom.css">
      <!-- Modernizr -->
      <script src="assets/js/modernizr.custom.js"></script>
      <!-- respond.js per IE8 -->
      <!--[if lt IE 9]>
      <script src="assets/js/respond.min.js"></script>
      <![endif]-->
	  <link rel="icon" href="../img/logo.ico" />
    </head>
    <body>
      <!-- Header e barra di navigazione -->
	 <!-- Header e barra di navigazione -->
  <header>
  <nav class="navbar navbar-default">
  <div class="container">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
    <!-- <a class="navbar-brand" href="index.html">Carlo</a> -->
   </div>
   <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav">
	  <li><a href="../../index.html">Home</a></li>
      <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
     </ul>
     <ul class="nav navbar-nav navbar-right">
		  <li><a href="situazione.php"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp; Dashboard</a></li>
          <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
   </div><!-- /.nav-collapse -->
  </div>
<?php
include 'db_connect.php';
include 'functions.php';
sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
if(isset($_POST['groupid'], $_POST['groupass'])){
	$_SESSION['groupid'] = $_POST['groupid'];
	$_SESSION['groupass'] = $_POST['groupass'];
}

// Altrimenti le credenziali sono nella sessione perche' provenienti da login.php


if(isset($_SESSION['groupid'], $_SESSION['groupass'])) {
	$mysqli;
	// Verifica la connessione
	if (mysqli_connect_errno())
	{
		echo "Connessione al database fallita: " . mysqli_connect_error();
	}else
	{
		// Login eseguito
		$logineseguito=1;
		$_SESSION['logineseguito']=1;
		/*$reg_user=$_POST['groupid'];
        $reg_pass=$_POST['groupass'];*/
        $mysqli;
		// Verifica la connessione
		if (mysqli_connect_errno())
		{
			echo "Connessione al database fallita: " . mysqli_connect_error();
		}
		else
		{   $_SESSION['valido']=0;
           	$sql = "SELECT * FROM gruppi where idgruppo='".$_SESSION['groupid']."' and groupass='".$_SESSION['groupass']."'";
           	if($result = mysqli_query($mysqli, $sql)){
    			if(mysqli_num_rows($result) > 0){
        			$group = mysqli_fetch_array($result);
					$_SESSION['valido']=1;
					//$_SESSION['groupass']=$reg_pass;
                    $_SESSION['surn1']=$group['cogn1'];$_SESSION['name1']=$group['nome1'];
                    $_SESSION['email1']=$group['email1'];
					$_SESSION['surn2']=$group['cogn1'];$_SESSION['name2']=$group['nome1'];
                    $_SESSION['email2']=$group['email1'];
					echo "	 <!-- Barra testata di pagina -->
					    	<div class='header-portfolio clearfix'>
							<h2 class='pull-left'>"."Portale del corso di Programmazione Classe-B: Area Riservata &#8594; ".
							$group['idgruppo'].
							/*" studenti: ".
							$group['cogn1']." ".$group['nome1']." - ".$group['cogn2']." ".
							$group['nome2']. */
							"</h2>
								</div>
								  </nav><!-- /.navbar -->
						</header><!-- /header -->
								<!-- /header-portfolio -->";

                    mysqli_free_result($result);//$conn->close();
                    $mysqli;
                    if ($mysqli->connect_error) {  die("Connessione fallita: " . $mysqli->connect_error);}
					$sql = "SELECT * FROM gruppi where idgruppo='".$_SESSION['groupid']."'";
					$result = $mysqli->query($sql);
					//$_SESSION['reg_user']=$reg_user;
                    $today = date('Y-m-d');
					// Visualizzazione integrale dei progetti da implementare
 					$mysqli;
                    if (mysqli_connect_errno())
					{
						echo "Errore durante la connessione: " . mysqli_connect_error();
					}
					$sql = "SELECT * FROM elaborati WHERE dataConsegna<='$today'";
					$result = $mysqli->query($sql);
					if ($result->num_rows > 0) {
						echo "<style>
        		table {

        				margin: 2.5%;
        				width: 95%;
        				box-shadow: 0 0 20px rgba(0,0,0,.4);
        				border: 1px solid #116b6a;
                }

            th {
                text-align: center;
                background-color: #b32929;
                color: white;
                font-weight: bold;
                }

        		th, td {
        				padding: 15px;
        				border: 1px solid;
        				text-align: center;
        				/*border-left: 0;
        				border-right: 0;
        			  }

        			  td {
        				background-color: white;
        			  }

.btn {
  border-radius: 1;
  background-color: #b32929;
  color: white;
}
        		</style>";
						$_SESSION['scripts']=array();

            echo "<strong><center><font size='6'><i>Elaborati consegnabili</i></font></center></strong>";

						echo "<br><table border='1' align='center' valign=''middle'>\n";

						echo "<tr><th>Id</th>
            <th>Titolo</th>
            <th>Testo</th>
							      <th>Data di consegna</th>
                    <th>Anno</th>
								  <th>Materiale</th>
                  <th>Note</th>
								  <th>Limite consegne</th>
                  <th></th></tr>";
						while($row = $result->fetch_assoc()) {
						    $instance=array();
							$v=$row['idelaborato'];

							echo "<tr>";
							$formatDate=date("d-m-Y", strtotime($row['dataConsegna']));
							$year=date('Y');
							$path='../../setup/'.$year.'/';
							$filePdf=$row['nomefile'].'.pdf';
							$Link=$path.$filePdf;
							/*echo "<script type='text/javascript'>
								function Link() {
									document.getElementById('Link').href = '$Link';
								}
								</script>";
							<td align='middle'><a href='#' id='Link' onclick='Link() target='_blank'>".$row['nomefile']."</a></td>
								*/
							array_push($instance,$row['idelaborato'],$row['titolo'],$row['anno'],$row['max_cons']);
							array_push($_SESSION['scripts'],$instance);
							echo "<td align='middle'>".$row['idelaborato']."</td>
							      <td>".$row['titolo']."</td>
							      <td><a href='$Link' target='_blank'>".$row['nomefile']."</a></td>
								  <td>".$formatDate."</td>
								  <td>".$row['anno']."</td>
								  <td>".$row['materiale']."</td>
								  <td>".$row['note']."</td>
								  <td>".$row['max_cons']."</td>
								  ";

							$v=	$row['idelaborato'];
                        	echo "<td align='middle'><a href='consegna.php?id=$v'>
								<button type='button' class='btn'>Consegna</button></a>";
                            echo "</td></tr>";
						}
                		echo "</table>\n";
         			}
            		mysqli_free_result($result); // libera la memoria
            		//mysqli_close($conn);
                    echo "<p></p>";
                }
                else
                {
						echo "<!DOCTYPE html>
						<!--[if IE 8]><html class='no-js lt-ie9' lang='en' ><![endif]-->
						<!--[if gt IE 8]><!--><html class='no-js' ><!--<![endif]-->
						<html>
						<head>
						<meta charset='utf-8'>
						<meta http-equiv='X-UA-Compatible' content='IE=edge'>
						<meta name='viewport' content='width=device-width, initial-scale=1.0'>
						<title>Unibo on Platform C</title>
						<!-- Fogli di stile -->
						<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
						<link rel='stylesheet' href='../bootstrap/css/bootstrap.css'>
						<link rel='stylesheet' href='../plugins/fancybox/jquery.fancybox.css'>
						<link rel='stylesheet' href='../plugins/flexslider/flexslider.css'>
						<link rel='stylesheet' href='../css/stili-custom.css'>
						<!-- Modernizr -->
						<script src='assets/js/modernizr.custom.js'></script>
						<!-- respond.js per IE8 -->
						<!--[if lt IE 9]>
						<script src='assets/js/respond.min.js'></script>
						<![endif]-->
						</head>
						<body>
						<!-- Header e barra di navigazione -->
						<header>

						</header>
						<div class='header-portfolio clearfix'>
						<h2 class='pull-left'>Unibo on platform C - Errore durante il Login: account non trovato! </h2>
						</div>
						</nav><!-- /.navbar -->
						</header><!-- /header -->";
                    	echo "<p><center><br><h3>Gruppo non presente<br> o password errata,<br> prego effettuare<br> la registrazione<br>
								oppure digitare<br> con attenzione<br><br> <font color='blue'><i>username</i> e <i>password</i></font><br><br>
							</h3><h4>Tra qualche istante sarai reindirizzato alla Home page</h4></center></p>";
						echo "<script>alert('Gruppo non presente o password errata, prego effettuare la registrazione
								oppure digitare con attenzione')</script>";
						//echo "<script>location.href='../../choose_login.php';</script>";
						//include("../../goto_chooselogin.php");
						echo"<noscript>
								<meta http-equiv='refresh' content='5;url=../../choose_login.php' />
							</noscript>";
						echo"<script type='text/javascript'>
								window.setTimeout(function() {
								window.location.href='../../choose_login.php';
								}, 5000);
							</script>";

                }
            }
        }




    }
  } else {
    // Le variabili corrette non sono state inviate a questa pagina dal metodo POST.
    echo"<script> alert( 'Email o Password non corretta!' );</script>";
  }
  ?>
<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>

  <!-- jQuery e plugin JavaScript  -->
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="../bootstrap/js/bootstrap.min.js"></script>
  <script src="../plugins/flexslider/jquery.flexslider.js"></script>
  <script src="../plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="../js/scripts.js"></script>
</body>
</html>
