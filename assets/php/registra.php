<html>
    <head>
	   <title>Portale Programmazione CL.B</title>
	   <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- <title>Consegna esercizi di programmazione</title> -->
<!-- Fogli di stile -->
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
<!--<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">-->
<link rel="stylesheet" href="../css/stili-custom.css">
<!-- Modernizr -->
<script src="../js/modernizr.custom.js"></script>

<!-- respond.js per IE8 -->
<!--[if lt IE 9]>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
<link rel="icon" href="assets/img/logo.ico" />
	</head>
    <body>
<!-- Header e barra di navigazione -->
<header>
<nav class="navbar navbar-default">
<div class="container">
 <div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
  </button>
  <!--<a class="navbar-brand" href="index.html">Carlo</a>-->
 </div>
 <div class="collapse navbar-collapse navbar-responsive-collapse">
   <ul class="nav navbar-nav">
    <li><a href="../../index.html">Home</a></li>
    <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
   </ul>
   <ul class="nav navbar-nav navbar-right">
        <li><a href="../../login.php"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
        <li><a href="login.php?registrato=0" style="text-decoration:none">Crea un gruppo</a></li>
  </ul>
 </div><!-- /.nav-collapse -->
</div>

<!-- Barra testata di pagina -->
<div class="header-portfolio clearfix">
 <h2 class="pull-left">Benvenuto sul portale del corso di Programmazione Classe-B</h2>
 <ul class="breadcrumb pull-right">
 <!-- 	<li><a href="../../index.html">Home</a></li>
  <li class="active">Login</li> -->
</ul>
</div><!-- /header-portfolio -->
</nav><!-- /.navbar -->
</header><!-- /header -->
<!-- Contenuti (griglia) -->
<div class="container">
<!-- Lead presentazione -->
<section id="presentazione">
<div class="row">
<div class="col-sm-12">

</div><!-- /.col-sm-12 -->
</div><!-- /.row -->
</section><!-- /section presentazione -->

</div><!-- container -->
</body>
</html>
	   <?php
	   	//session_start();
		function sendmail($email,$surn,$name){
			$toclient = $email;
			$sbjclient = "Hai ricevuto una mail dal sito ";
			$msgclient = "Ciao $surn $name, ti informiamo che la registrazione
			al gruppo ".$_SESSION['groupid']." &egrave; avvenuta con successo, ricorda di accedere
			alla piattaforma mediante la password da te indicata: ".$_SESSION['groupass'];

			$fromclient = "Unibo On Platform C";
			$headersclient = 'MIME-Version: 1.0' . "\n";
			$headersclient .= 'Content-type: text/html; charset=iso-8859-1' . "\n";
			$headersclient .= "From: '$fromclient'";
			mail($toclient, $sbjclient, $msgclient, $headersclient);
		}


		function notify($email1,$surn1,$name1,$email2,$surn2,$name2){
			sendmail($email1,$surn1,$name1);
			sendmail($email2,$surn2,$name2);

			$msg="Benvenuti, il vostro username &egrave; ".$_SESSION['groupid'].".<br>Le credenziali sono state inviate via email.<br>
			Tra qualche istante sarete reindirizzati alla pagina di consegna degli  elaborati.";

			echo "<font face='verdana'><center><h1>".$msg."</h1></center></font>";
			echo"<noscript>
					<meta http-equiv='refresh' content='5;url=process_login.php' />
				</noscript>";
			echo"<script type='text/javascript'>
					window.setTimeout(function() {
					window.location.href='process_login.php';
					}, 6000);
				</script>";
		}

		function recovey($email){
			include 'db_connect.php';
			$mysqli;

			$sql = "SELECT * FROM gruppi WHERE email1='$email' or email2='$email'";
			if($result = mysqli_query($mysqli, $sql)){
				if(mysqli_num_rows($result) > 0){
					$gruppo = mysqli_fetch_array($result);
					echo "<h1 align='center'>Unibo On Platform C</h1>
					<h3 align='center'><i>Recupero credenziali</i></h3>";
					$to=$email;
					$subject='Unibo On Platform C - reinvio password';
					$message="Gentile studente " ;
					$message.="Ti inviamo l'id del tuo gruppo: ".$gruppo['groupid']." e la relativa password: "
					.$gruppo['groupass'];
					$headers='From: Unibo On Platform C' . "\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
					mail($to, $subject, $message, $headers);
					echo "<script>alert('Inoltro effettuato, come richiesto')</script>";

				}
			}

			echo"<button onclick='goHome()'>Replace document</button>
					<script>
					function goHome() {
					window.location.replace('../../index.html')
					}
					</script>";
		}

		function mesEsiste() {
			//echo "utente esistente";
			if(isset($_POST['sendmail']) && !empty($_POST['sendmail']))
			{
				$email=$_POST['rec_email'];
				recovey($email);
			}
			else{
				echo "<script>alert('Attenzione, iscrizione esistente per uno o tutti gli utenti')</script>
				  <div class='login-page'>
				<div class='form'>
				<form class='login-form' action='recupera.php' method='post' name='login_form'>
				<input type='submit' name='sendmail' value='Recupera le credenziali e torna alla Home page'>
				</form>
				</div>
				</div>";
			}
		}

		if (isset($_POST['proceed']))
        {
			include 'db_connect.php';
			include 'functions.php';
			sec_session_start();

			$surn1=$_POST['surn1'];
			$name1=$_POST['name1'];
			$email1=$_POST['email1'];
			$matr1=$_POST['matr1'];
			$surn2=$_POST['surn2'];
			$name2=$_POST['name2'];
			$email2=$_POST['email2'];
			$matr2=$_POST['matr2'];
			$groupass=$_POST['groupass'];

			if (strcmp($email1,$email2)!=0)
				{
					$mysqli;


			$sql = "SELECT matr1,matr2,email1,email2 FROM gruppi WHERE email1='$email1' or email1='$email2' or email2='$email1' or email2='$email2'
					or matr1='$matr1' or matr1='$matr2' or matr2='$matr1' or matr2='$matr2'";
            if($result = mysqli_query($mysqli, $sql)){
    			if(mysqli_num_rows($result) > 0){
					echo "<script>alert('Attenzione, iscrizione esistente per uno o entrambi gli utenti')</script>
					<div class='form'>
					<form method='post' action='recupera.php'>
					<table border='0' align='center'>
					<tr><td><p><fieldset>
						<legend><center>Richiedi ora le credenziali, oppure esegui nuovamente il login</center></legend>
						Inserisci la tua email&nbsp;&nbsp;&nbsp;<input type='email' name='rec_email' size='45' required><br>
						</fieldset>
					</p></td></tr>
					<tr><td><center><input type='submit' name='sendmail' value='Recupera le credenziali'></center></td></tr>
					</table>
					</form>
					</div>
					";
                }
                else
			    {
					//Utente non registrato e, quindi , richiesta di registrazione consentita
                    mysqli_free_result($result);
                	//$mysqli->close();
					$mysqli;
					if ($mysqli->connect_error) {  die("Connessione fallita: " . $conn->connect_error);}
					$sql = "SELECT * FROM gruppi";
					$result = mysqli_query($mysqli,$sql);

					if ($result)
					{
						$count = mysqli_num_rows($result);
						mysqli_free_result($result);
					}
					//$mysqli->close();

					$year=date('Y');
					$count++;
					$curId="gruppo".$count;
					$mysqli;
					if ($mysqli->connect_error) {
						die("Impossibile connettersi al database: " . $mysqli->connect_error);
					}
					$sql="INSERT INTO gruppi (idgruppo,anno,groupass,cogn1,nome1,email1,matr1,cogn2,nome2,email2,matr2)".
						 "VALUES ('".$curId."','".$year."','".$groupass."','".$surn1."','".$name1."','".$email1."',
						  '".$matr1."','".$surn2."','".$name2."','".$email2."','".$matr2."')";
					if($result = mysqli_query($mysqli, $sql))
					{
						// checking whether file exists or not
						$yearFolder = '../../consegne/'.$year;
						if (!file_exists($yearFolder))
						{
							mkdir($yearFolder, 0777);// create year folder
						}
						$groupFolder = $yearFolder.'/'.$curId;
						if (!file_exists($groupFolder))
						{
							mkdir($groupFolder, 0777);// create group folder into year folder
						}

						$_SESSION['groupid']=$curId;
						$_SESSION['groupass']=$groupass;
						notify($email1,$surn1,$name1,$email2,$surn2,$name2);
						$mysqli->close();
					}
					else {echo "Errore: " . $sql . "<br>" . $mysqli->error;}
               }
    		}
		  }
		  else {

				 echo "<script type='text/javascript'>
					alert('Errore, email coincidenti!')
					</script>";
		  }
		}

		else {
				// codice se submit non premuto
		}

?>

<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>
  <!-- jQuery e plugin JavaScript  -->
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
  <script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="assets/plugins/validation/jquery.validate.js"></script>
  <script src="assets/js/scripts.js"></script>
  <script type="text/javascript" src="assets/js/sha512.js"></script>
  <script type="text/javascript" src="assets/js/forms.js"></script>
</body>
</html>
