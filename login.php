<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Portale Programmazione CL.B</title>
<!-- Fogli di stile -->
<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">

<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
<link rel="stylesheet" href="assets/css/stili-custom.css">

<!--/ da cartella PWD  -->
	<!--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>

<!-- Modernizr -->
<script src="assets/js/modernizr.custom.js"></script>
<!-- respond.js per IE8 -->
<!--[if lt IE 9]>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
<link rel="icon" href="assets/img/logo.ico" />
</head>
<body>
  <!-- Header e barra di navigazione -->
  <header>
  <nav class="navbar navbar-default">
  <div class="container">
   <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
     <span class="icon-bar"></span>
    </button>
   </div>
   <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav">
	 	 	<li><a href="index.html">Home</a></li>
      <li><a href="https://www.unibo.it/it/didattica/insegnamenti/insegnamento/2019/396867">Pagina del corso</a></li>
     </ul>

	 </ul>
  <ul class="nav navbar-nav navbar-right">
 <li><a href="login.php?registrato=1" style="text-decoration:none"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
 <li><a href="login.php?registrato=0" style="text-decoration:none">Crea un gruppo</a></li>
  </ul>

   </div><!-- /.nav-collapse -->
  </div>


<?php
$registrato=$_REQUEST['registrato'];
//$registrato=$_SESSION['registrato'];
if ($registrato==1)
{
	echo"<!-- Barra testata di pagina -->
 <div class='header-portfolio clearfix'>
    <h2 class='pull-left'>Portale Programmazione CL.B - Accedi per consegnare gli elaborati</h2>
 </div>
  </nav><!-- /.navbar -->
  </header><!-- /header -->
<!-- /header-portfolio -->
<div class='container'>
  <div class='login-page'>
  <div class='form'>
   <form class='login-form' action='assets/php/process_login.php' method='post' name='login_form'>
	  <div class='form-group'>
      <input type='text' name='groupid' class='form-control' placeholder='username' required maxlength='20'/>
		<input type='password' id='password' name='groupass'  class='form-control' data-toggle='password' placeholder='password' required maxlength='20'/>
		</div>
		<script type='text/javascript'>
			$('#password').password('toggle');
		</script>
      <input type='submit' value='Login'/>
       <!-- onclick='formhash(this.form, this.form.password);'-->
     <p class='message'>Non sei registrato? <a href='login.php?registrato=0'>Crea un nuovo gruppo</a></p>
    </form>
 </div>
</div>";
}
else{
	echo"<!-- Barra testata di pagina -->
 <div class='header-portfolio clearfix'>
  <h2 class='pull-left'>Portale Programmazione CL.B - Registra il tuo gruppo per consegnare gli elaborati</h2>
  </div>
<!-- /header-portfolio -->
  <div class='login-page'>
  <div class='form'>
    <form class	='login-form'  name='register-form'   action='assets/php/registra.php' method='post'>
	<fieldset>
		<legend>Dati studente 1</legend>
	  <input type='text' placeholder='matricola' name='matr1' required maxlength='20'/>
      <input type='text' placeholder='nome' name='name1' required maxlength='32'/>
	  <input type='text' placeholder='cognome' name='surn1' required maxlength='32'/>
      <input type='email' placeholder='indirizzo email' name='email1' required maxlength='40'/>
	</fieldset>
	<fieldset>
		<legend>Dati studente 2</legend>
	  <input type='text' placeholder='matricola' name='matr2' maxlength='20'/>
      <input type='text' placeholder='nome' name='name2' maxlength='32'/>
	  <input type='text' placeholder='cognome' name='surn2' maxlength='32'/>
      <input type='email' placeholder='indirizzo email' name='email2' maxlength='40'/>
	</fieldset>
	<fieldset>
	<legend>Imposta una password</legend>
		<input type='password' id='password' name='groupass'  class='form-control' data-toggle='password' placeholder='password' required maxlength='20'/>
		<script type='text/javascript'>
			$('#password').password('toggle');
		</script>
	</fieldset>
	<legend></legend>
      <input type='submit' name='proceed' value='Crea gruppo' />
      <p class='message'>Sei gi&agrave; registrato? <a href='login.php?registrato=1'>Entra</a></p>
    </form>

  </div>
</div>
  ";
}
?>
</div>
<!-- Footer -->
<footer>
<section id="footer-navigazione">
<div class="row">
<div class="col-sm-4">
 <h3>Contatti</h3>
 <address>
   <strong>E-mail</strong><br>
   <a href="mailto:">alessandra.lumini@unibo.it</a>
 </address>
 <address>
   <strong>Portale di consegna esercizi di programmazione</strong><br>
   Dipartimento di Informatica - Scienza e Ingegneria<br>
   Via dell'Universit&agrave;&nbsp;50, Cesena <br><a href="https://www.unibo.it/uniboWeb/unibomappe/default.aspx?kml=%2fUniboWeb%2fStruct.kml%3fStrID%3d3562">Vai alla mappa</a>
 </address>
</div>
</div>
</section>
<section id="footer-copy">
<div class="row">
<div class="col-sm-12">
 <p class="right"><a href="https://www.unibo.it/it/ateneo/privacy-e-note-legali/privacy/informative-sul-trattamento-dei-dati-personali">Privacy</a></p>
</div>
</div>
</section>
</footer>
  <!-- jQuery e plugin JavaScript  -->
  <script src="http://code.jquery.com/jquery.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/plugins/flexslider/jquery.flexslider.js"></script>
  <script src="assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
  <script src="assets/plugins/validation/jquery.validate.js"></script>
  <script src="assets/js/scripts.js"></script>
  <script type="text/javascript" src="assets/js/sha512.js"></script>
  <script type="text/javascript" src="assets/js/forms.js"></script>
</body>
</html>
